//
//  Score.swift
//  Football Data
//
//  Created by Oluwaniran Olorunfemi on 17/12/2020.
//

import Foundation


struct Score : Codable {
    var fullTime : Goals
    var halfTime : Goals
    var extraTime : Goals
}

struct Goals : Codable {
    var homeTeam : Int?
    var awayTeam : Int?
}
