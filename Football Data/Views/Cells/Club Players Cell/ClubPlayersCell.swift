//
//  ClubPlayersCell.swift
//  Football Data
//
//  Created by Oluwaniran Olorunfemi on 15/12/2020.
//

import UIKit
class ClubPlayersCell : UITableViewCell {
    
    
    @IBOutlet weak var serialNumberLabel: UILabel!
    @IBOutlet weak var playerNameLabel: UILabel!
    @IBOutlet weak var playerPositionLabel: UILabel!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
}
