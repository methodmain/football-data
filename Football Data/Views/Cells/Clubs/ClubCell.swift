//
//  ClubCell.swift
//  Football Data
//
//  Created by Oluwaniran Olorunfemi on 14/12/2020.
//

import UIKit

class ClubCell : UICollectionViewCell {
    
    @IBOutlet weak var clubLogo: UIImageView!
    @IBOutlet weak var clubName: UILabel!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
}
