//
//  ClubPlayersController.swift
//  Football Data
//
//  Created by Oluwaniran Olorunfemi on 15/12/2020.
//

import UIKit
import XLPagerTabStrip
import SkeletonView

class ClubPlayerController : UIViewController {
    private let tableView = UITableView()
    
    var team : StandingTeam?
    var players = [Players]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        fetchPlayers()
    }
    
    func setup() {
        self.title = team?.name
        tableView.register(UINib(nibName: "ClubPlayersCell", bundle: nil), forCellReuseIdentifier: "ClubPlayersCell")
        tableView.register(UINib(nibName: "ClubHeaderCell", bundle: nil), forHeaderFooterViewReuseIdentifier: "ClubHeaderCell")
        
        view.backgroundColor = UIColor.white
        view.addSubview(tableView)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height - 65)
        //tableView.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 0, right: 0)
        tableView.layoutMargins = UIEdgeInsets.zero
        tableView.separatorInset = UIEdgeInsets.zero
        tableView.tableFooterView = UIView()
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.isSkeletonable = true
        //tableView.alwaysBounceVertical = false
        tableView.bounces = false
    }
    
    func fetchPlayers() {
        guard let selectedTeam = team else { return AlertView.showError(view: self, title: "Error", message: "No Team selected")}
        
        self.tableView.showAnimatedGradientSkeleton()
        
        ServiceGenerator.shared.getPlayers(teamId: selectedTeam.id) { (result) in
            switch result {
            case let .success(response):
                let club = response as! TeamSquad
                self.players = club.squad
                self.tableView.reloadData()
                DispatchQueue.main.async {
                    //refresh table on main queue
                    self.tableView.reloadData()
                }
                self.tableView.hideSkeleton()
                break
            case let .failure(error):
                self.tableView.hideSkeleton()
                AlertView.showError(view: self, title: "Error", message: error.localizedDescription)
            }
        }
    }
}

extension ClubPlayerController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return players.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ClubHeaderCell") as! ClubHeaderCell
        headerView.clubLogo.sd_setImage(with: URL(string: team?.crestUrl ?? "pencil.circle.fill"),placeholderImage : UIImage(named: "defaultImage"),completed: nil)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70
    }
}

extension ClubPlayerController : SkeletonTableViewDataSource {
//    func numSections(in collectionSkeletonView: UITableView) -> Int {
//        return 1
//    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "ClubPlayersCell"
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ClubPlayersCell") as! ClubPlayersCell
        cell.serialNumberLabel.text = "\(indexPath.row + 1)"
        cell.playerNameLabel.text = players[indexPath.row].name
        cell.playerPositionLabel.text = players[indexPath.row].position
        return cell
    }
}
