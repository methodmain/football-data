//
//  CompetitionTeams.swift
//  Football Data
//
//  Created by Oluwaniran Olorunfemi on 18/12/2020.
//

import Foundation

struct CompetitionTeams : Codable {
    var teams : [StandingTeam]
}
