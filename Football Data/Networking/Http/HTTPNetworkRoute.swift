//
//  HTTPNetworkRoute.swift
//  Football Data
//
//  Created by Oluwaniran Olorunfemi on 17/12/2020.
//

struct HTTPNetworkRoute {
    
    static let BASE_URL = "https://api.football-data.org/v2/"
    static let MATCHES = "\(BASE_URL)matches"
    static let COMPETITIONS = "\(BASE_URL)competitions"
    static let TEAMS = "\(BASE_URL)teams"
    
}


