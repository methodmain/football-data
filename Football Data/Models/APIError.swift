//
//  APIError.swift
//  Football Data
//
//  Created by Oluwaniran Olorunfemi on 17/12/2020.
//

struct APIError : Codable {
    var message : String
    var errorCode : Int
}
