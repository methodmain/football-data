//
//  NSLocale.swift
//  Football Data
//
//  Created by Oluwaniran Olorunfemi on 18/12/2020.
//

import Foundation

extension NSLocale {
    class func is12HourFormat() -> Bool {
        let locale = self.current
        let dateFormat = DateFormatter.dateFormat(fromTemplate: "j", options: 0, locale: locale)!
        if dateFormat.contains("a") {
            return true
        }
        else {
            return false
        }
    }
}
