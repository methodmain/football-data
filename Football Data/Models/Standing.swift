//
//  Standing.swift
//  Football Data
//
//  Created by Oluwaniran Olorunfemi on 17/12/2020.
//

import Foundation

struct Standing : Codable {
    var position: Int
    var playedGames : Int
    var won : Int
    var draw : Int
    var lost : Int
    var points : Int
    var goalsFor : Int
    var goalsAgainst : Int
    var goalDifference : Int
    var team : StandingTeam
}

struct StandingTeam : Codable {
    var id : Int
    var name : String
    var crestUrl : String
}

struct Standings : Codable {
    var data : [StandingList]
    
    enum CodingKeys: String, CodingKey {
        case data = "standings"
    }
}

struct StandingList : Codable {
    var table : [Standing]
}
