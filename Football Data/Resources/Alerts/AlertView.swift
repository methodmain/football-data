//
//  AlertView.swift
//  Football Data
//
//  Created by Oluwaniran Olorunfemi on 17/12/2020.
//

import UIKit

class AlertView: NSObject {
    class func showError(view: UIViewController , title: String , message: String){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))
        
        DispatchQueue.main.async {
            view.present(alert, animated: true, completion: nil)
        }
    }
    
    class func showSuccess(view: UIViewController , title: String , message: String){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))
        
        DispatchQueue.main.async {
            view.present(alert, animated: true, completion: nil)
        }
    }
}
