# Football APP

This app demo displays football fixtures and consumes data from  https://www.football-data.org

![alt text](screenshot1.png)
![alt text](screenshot2.png)
![alt text](screenshot3.png)
![alt text](screenshot4.png)
![alt text](screenshot5.png)
![alt text](screenshot6.png)
![alt text](screenshot7.png)
![alt text](screenshot8.png)


## Requirements
Xcode 12.1

<br /> <br />

## Stack

IOS - Swift

<br /> <br />

### How to install <br>
	*Clone Repo <br>
	*Build and Run!

<br /><br />


