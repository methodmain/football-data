//
//  CompetitionsViewController.swift
//  Football Data
//
//  Created by Oluwaniran Olorunfemi on 13/12/2020.
//

import UIKit
import SkeletonView
import SVProgressHUD

class CompetitionsViewController: UIViewController {
    
    private let tableView = UITableView()
    var competitions = [Competition]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup() {
        self.title = "Competitions"
        tableView.register(UINib(nibName: "ClubPlayersCell", bundle: nil), forCellReuseIdentifier: "ClubPlayersCell")
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "tableCell")
        view.addSubview(tableView)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.frame = view.bounds
        tableView.layoutMargins = UIEdgeInsets.zero
        tableView.separatorInset = UIEdgeInsets.zero
        tableView.showsVerticalScrollIndicator = false
        tableView.tableFooterView = UIView()
        tableView.isSkeletonable = true
        fetchCompetitions()
    }
    
    func fetchCompetitions() {
        self.tableView.showAnimatedGradientSkeleton()
        ServiceGenerator.shared.getCompetitions { (result) in
            switch result {
            case let .success(response):
                let allcompetitions = response as! Competitions
                self.competitions = allcompetitions.all
                self.tableView.reloadData()
                self.tableView.hideSkeleton()
                break
            case let .failure(error):
                self.tableView.hideSkeleton()
                AlertView.showError(view: self, title: "Error", message: error.localizedDescription)
            }
        }
    }
}

extension CompetitionsViewController : SkeletonTableViewDataSource {
    
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "ClubPlayersCell"
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return competitions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath)
        cell.textLabel?.text = competitions[indexPath.row].name
        cell.isSkeletonable = true
        cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
        cell.textLabel?.textColor = UIColor.black
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    
}


extension CompetitionsViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.row < competitions.count else {
            return AlertView.showError(view: self, title: "Error", message: "No league information found")
        }
        
        competitionSelectAction(indexPath.row)
    }
    
    func competitionSelectAction(_ competitionId : Int) {
        let league = competitions[competitionId]
        
        //fetch league details
        ServiceGenerator.shared.getStanding(competitonId: league.id) { (result) in
            SVProgressHUD.show()
            switch result {
            case let .success(response):
                SVProgressHUD.dismiss()
                let standings = response as! Standings
                let data = standings.data
                let standingList = (data.count > 0) ? data[0].table : [Standing]()
               
                DispatchQueue.main.async {
                    let leagueDetailsScreen = LeagueNavbarController(nibName: nil, bundle: nil)
                    leagueDetailsScreen.hidesBottomBarWhenPushed = true
                    leagueDetailsScreen.league = league
                    leagueDetailsScreen.table = standingList
                    self.navigationController?.pushViewController(leagueDetailsScreen, animated: true)
                }
                break
            case let .failure(error):
                SVProgressHUD.dismiss()
                self.tableView.reloadData()
                AlertView.showError(view: self, title: "Error", message: error.localizedDescription)
            }
        }
    }
}
