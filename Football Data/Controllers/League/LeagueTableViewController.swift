//
//  LeagueTableViewController.swift
//  Football Data
//
//  Created by Oluwaniran Olorunfemi on 14/12/2020.
//

import UIKit
import XLPagerTabStrip
import SDWebImageSVGCoder

class LeagueTableViewController : UIViewController {
    private let tableView = UITableView()
    var table : [Standing]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "LeagueTableCell", bundle: nil), forCellReuseIdentifier: "LeagueTableCell")
        view.addSubview(tableView)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height - 105) //view.bounds
        //tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: -20, right: 0)
        tableView.layoutMargins = UIEdgeInsets.zero
        tableView.separatorInset = UIEdgeInsets.zero
        tableView.tableFooterView = UIView()
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        //tableView.alwaysBounceVertical = false
        tableView.bounces = false
        tableView.reloadData()
    }
}

extension LeagueTableViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return table?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeagueTableCell") as! LeagueTableCell
        cell.positionLabel.text = ""
        cell.clubNameLabel.text = "Club"
        cell.pointsLabel.text = "PTS"
        cell.gamesDifferenceLabel.text = "GD"
        cell.gamesPlayedLabel.text = "P"
        cell.contentView.backgroundColor = UIColor.lightGray
        cell.clubLogo.image = UIImage()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
}

extension LeagueTableViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeagueTableCell", for: indexPath) as! LeagueTableCell
        cell.positionLabel.text = "\(table![indexPath.row].position)"
        cell.clubNameLabel.text = "\(table![indexPath.row].team.name)"
        cell.pointsLabel.text = "\(table![indexPath.row].points)"
        cell.gamesDifferenceLabel.text = "\(table![indexPath.row].goalDifference)"
        cell.gamesPlayedLabel.text = "\(table![indexPath.row].playedGames)"
        cell.clubLogo.sd_setImage(with: URL(string: table![indexPath.row].team.crestUrl),placeholderImage : UIImage(named: "defaultImage"),completed: nil)
        //cell.clubLogo.image = UIImage()
        return cell
    }
}

extension LeagueTableViewController : IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Table")
    }
}
