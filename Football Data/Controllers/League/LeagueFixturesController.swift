//
//  LeagueFixturesController.swift
//  Football Data
//
//  Created by Oluwaniran Olorunfemi on 18/12/2020.
//

import UIKit
import XLPagerTabStrip
import SkeletonView


class LeagueFixturesController : UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var noResultView: UIView!
    
    var league : Competition?
    var matches = [Match]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup() {
        collectionView?.register(UINib(nibName: "FixtureCell", bundle: nil), forCellWithReuseIdentifier: "FixtureCell")
    }
    
    
    @IBAction func retryAction(_ sender: Any) {
        fetchMatches()
    }
    
    func fetchMatches() {
        guard let selectedLeague = league else { return AlertView.showError(view: self, title: "Error", message: "No Club selected")}
        
        self.noResultView.isHidden = true
        self.collectionView?.showAnimatedGradientSkeleton()
        
        ServiceGenerator.shared.getMatches(competitonId: selectedLeague.id) { (result) in
            switch result {
            case let .success(response):
                let allMatches = response as! Matches
                let matchBuffer = allMatches.all
                self.matches = matchBuffer
                self.collectionView?.reloadData()
                self.collectionView.isHidden = (matchBuffer.count == 0)
                self.noResultView.isHidden = (matchBuffer.count > 0)
                self.collectionView?.hideSkeleton()
                break
            case let .failure(error):
                self.collectionView.isHidden = true
                self.noResultView.isHidden = false
                self.collectionView?.hideSkeleton()
                AlertView.showError(view: self, title: "Error", message: error.localizedDescription)
            }
        }
    }
}

extension LeagueFixturesController : SkeletonCollectionViewDataSource {
    
    func collectionSkeletonView(_ skeletonView: UICollectionView, cellIdentifierForItemAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "FixtureCell"
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return matches.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FixtureCell", for: indexPath) as! FixtureCell
        cell.awayTeamLabel.text = matches[indexPath.row].awayTeam.name
        cell.homeTeamLabel.text = matches[indexPath.row].homeTeam.name
        cell.matchDateLabel.text = "MD: \(matches[indexPath.row].matchDay)"
        cell.timeLabel.text = matches[indexPath.row].date.getDate()?.formattedTime
        
        let homeTeamScore = matches[indexPath.row].getCurrentScore().homeTeam
        let awayTeamScore = matches[indexPath.row].getCurrentScore().awayTeam
        cell.homeTeamScoreLabel.text = homeTeamScore != nil ? "\(homeTeamScore!)" : "-"
        cell.awayTeamScoreLabel.text = awayTeamScore != nil ? "\(awayTeamScore!)" : "-"
        return cell
    }
    
}

extension LeagueFixturesController : UIScrollViewDelegate{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension LeagueFixturesController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 80)
    }
}

extension LeagueFixturesController : IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Fixtures")
    }
}
