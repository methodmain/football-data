//
//  LeagueTableCell.swift
//  Football Data
//
//  Created by Oluwaniran Olorunfemi on 15/12/2020.
//

import UIKit

class LeagueTableCell : UITableViewCell {
    
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var clubLogo: UIImageView!
    @IBOutlet weak var clubNameLabel: UILabel!
    @IBOutlet weak var gamesPlayedLabel: UILabel!
    @IBOutlet weak var gamesDifferenceLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
}

