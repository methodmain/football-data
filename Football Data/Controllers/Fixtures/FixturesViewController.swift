//
//  FixturesViewController.swift
//  Football Data
//
//  Created by Oluwaniran Olorunfemi on 13/12/2020.
//

import UIKit
import SkeletonView
import XLPagerTabStrip

class FixturesViewController : UIViewController {
    
    //@IBOutlet weak var collectionView: UICollectionView!
    private var collectionView : UICollectionView?
    
    var matches = [Match]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup() {
        collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height), collectionViewLayout: UICollectionViewFlowLayout.init())
        view.addSubview(collectionView!)
        collectionView?.dataSource = self
        collectionView?.delegate = self
        collectionView?.backgroundColor = UIColor.white
        collectionView?.contentInset = UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
        collectionView?.register(UINib(nibName: "FixtureCell", bundle: nil), forCellWithReuseIdentifier: "FixtureCell")
        collectionView?.showsVerticalScrollIndicator = false
        collectionView?.isSkeletonable = true
        self.title = "Today's Fixtures"
        fetchMatches()
    }
    
    func fetchMatches() {
        self.collectionView?.showAnimatedGradientSkeleton()
        ServiceGenerator.shared.getMatches { (result) in
            switch result {
            case let .success(response):
                let allMatches = response as! Matches
                self.matches = allMatches.all
                self.collectionView?.reloadData()
                self.collectionView?.hideSkeleton()
                break
            case let .failure(error):
                self.collectionView?.hideSkeleton()
                AlertView.showError(view: self, title: "Error", message: error.localizedDescription)
            }
        }
    }
}

extension FixturesViewController : SkeletonCollectionViewDataSource {
    
    func collectionSkeletonView(_ skeletonView: UICollectionView, cellIdentifierForItemAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "FixtureCell"
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return matches.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FixtureCell", for: indexPath) as! FixtureCell
        cell.awayTeamLabel.text = matches[indexPath.row].awayTeam.name
        cell.homeTeamLabel.text = matches[indexPath.row].homeTeam.name
        cell.matchDateLabel.text = "MD: \(matches[indexPath.row].matchDay)"
        cell.timeLabel.text = matches[indexPath.row].date.getDate()?.formattedTime
        
        let homeTeamScore = matches[indexPath.row].getCurrentScore().homeTeam
        let awayTeamScore = matches[indexPath.row].getCurrentScore().awayTeam
        cell.homeTeamScoreLabel.text = homeTeamScore != nil ? "\(homeTeamScore!)" : "-"
        cell.awayTeamScoreLabel.text = awayTeamScore != nil ? "\(awayTeamScore!)" : "-"
        return cell
    }
    
}

extension FixturesViewController : UIScrollViewDelegate{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension FixturesViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 80)
    }
}

extension FixturesViewController : IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Fixture")
    }
}
