//
//  HTTPMethod.swift
//  Football Data
//
//  Created by Oluwaniran Olorunfemi on 17/12/2020.
//

public enum HTTPMethod: String{
    
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case patch = "PATCH"
    case delete = "DELETE"
}
