//
//  HTTPNetworkResponse.swift
//  Football Data
//
//  Created by Oluwaniran Olorunfemi on 17/12/2020.
//
import Foundation
import Alamofire

struct HTTPNetworkResponse {
    
    // Properly checks and handles api response and catches error
    static func handleNetworkResponse<T: Decodable>(model : T.Type, for response: AFDataResponse<Data?>) -> Result<Any> {
        
        guard let result = response.data else {
            return Result.failure(HTTPNetworkError.UnwrappingError)
        }

        guard let statusCode = response.response?.statusCode else {
            return Result.failure(HTTPNetworkError.UnwrappingError)
        }
        
        do {
            return .success(try JSONDecoder().decode(T.self, from: result))
        } catch {
            do {
                let apiError = try JSONDecoder().decode(APIError.self, from: result)
                return .failure(NSError(domain: "", code: apiError.errorCode, userInfo: [NSLocalizedDescriptionKey: "\(apiError.message)"]))
            } catch {
                return catchError(statusCode: statusCode)
            }
        }
    }
    
    
    static func catchError(statusCode: Int) -> Result<Any> {
        switch statusCode {
            case 400:
                return .failure(NSError(domain: "", code: statusCode, userInfo: [NSLocalizedDescriptionKey: "Bad Request"]))
            case 403:
                return .failure(NSError(domain: "", code: statusCode, userInfo: [NSLocalizedDescriptionKey: "Restricted Access"]))
            case 404:
                return .failure(NSError(domain: "", code: statusCode, userInfo: [NSLocalizedDescriptionKey: "Not found"]))
            case 429:
                return .failure(NSError(domain: "", code: statusCode, userInfo: [NSLocalizedDescriptionKey: "Too many requests"]))
            default: return .failure(NSError(domain: "", code: statusCode, userInfo: [NSLocalizedDescriptionKey: "An unknown error occured"]))
        }
    }
    
}
