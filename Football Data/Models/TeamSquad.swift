//
//  TeamSquad.swift
//  Football Data
//
//  Created by Oluwaniran Olorunfemi on 18/12/2020.
//

import Foundation

struct TeamSquad : Codable {
    var squad : [Players]
}

struct Players : Codable {
    var id : Int
    var name : String
    var position : String?
}
