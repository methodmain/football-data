//
//  LeagueTeamsViewController.swift
//  Football Data
//
//  Created by Oluwaniran Olorunfemi on 14/12/2020.
//

import UIKit
import XLPagerTabStrip
import SkeletonView

class LeagueTeamsViewController : UIViewController {
    private var collectionView : UICollectionView?
    
    var league : Competition?
    var clubs = [StandingTeam]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height - 105), collectionViewLayout: UICollectionViewFlowLayout.init())
        collectionView?.register(UINib(nibName: "ClubCell", bundle: nil), forCellWithReuseIdentifier: "ClubCell")
        view.addSubview(collectionView!)
        collectionView?.dataSource = self
        collectionView?.delegate = self
        collectionView?.backgroundColor = UIColor.white
        collectionView?.contentInset = UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
        collectionView?.showsHorizontalScrollIndicator = false
        collectionView?.showsVerticalScrollIndicator = false
        collectionView?.isSkeletonable = true
        
        fetchClubs()
    }
    
    func fetchClubs() {
        guard let selectedLeague = league else { return AlertView.showError(view: self, title: "Error", message: "No Club selected")}
        
        self.collectionView?.showAnimatedGradientSkeleton()
        
        ServiceGenerator.shared.getTeams(competitonId: selectedLeague.id) { (result) in
            switch result {
            case let .success(response):
                let competitionTeams = response as! CompetitionTeams
                self.clubs = competitionTeams.teams
                self.collectionView?.reloadData()
                self.collectionView?.hideSkeleton()
                break
            case let .failure(error):
                self.collectionView?.hideSkeleton()
                AlertView.showError(view: self, title: "Error", message: error.localizedDescription)
            }
        }
    }
}


extension LeagueTeamsViewController : SkeletonCollectionViewDataSource {
    
    func collectionSkeletonView(_ skeletonView: UICollectionView, cellIdentifierForItemAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "ClubCell"
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return clubs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ClubCell", for: indexPath) as! ClubCell
        cell.clubName.text = clubs[indexPath.row].name
        cell.clubLogo.sd_setImage(with: URL(string: clubs[indexPath.row].crestUrl),placeholderImage : UIImage(named: "defaultImage"),completed: nil)
        return cell
    }
    
}

extension LeagueTeamsViewController : UIScrollViewDelegate{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        return flowLayout.minimumInteritemSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard indexPath.row < clubs.count else { return }
        selectTeamAction(selectedClub: clubs[indexPath.row])
    }
    
    func selectTeamAction(selectedClub : StandingTeam) {
        DispatchQueue.main.async {
            let playersScreen = ClubPlayerController(nibName: nil, bundle: nil)
            playersScreen.team = selectedClub
            self.navigationController?.pushViewController(playersScreen, animated: true)
        }
    }
}

extension LeagueTeamsViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let numberofItem: CGFloat = 3
        let collectionViewWidth = collectionView.bounds.width
        let extraSpace = (numberofItem - 1) * flowLayout.minimumInteritemSpacing
        let inset = flowLayout.sectionInset.right + flowLayout.sectionInset.left
        let width = Int((collectionViewWidth - extraSpace - inset - 10) / numberofItem)
        
        return CGSize(width: width, height: width)
    }
}

extension LeagueTeamsViewController : IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Teams")
    }
}
