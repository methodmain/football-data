//
//  ServiceGenerator.swift
//  Football Data
//
//  Created by Oluwaniran Olorunfemi on 17/12/2020.
//

import Foundation
import Alamofire

struct ServiceGenerator {
    static let shared = ServiceGenerator()
    static let bearerToken = "6c6045f25e2b4510b3957e218cb41c80"
    
    static func getHttpHeader() -> HTTPHeaders {
        let headers : HTTPHeaders = ["X-Auth-Token": bearerToken,"Accept":"application/json"]
        return headers
    }
    
    func getMatches(_ completion: @escaping (Result<Any>) -> ()) {
        AF.request(HTTPNetworkRoute.MATCHES, method: .get, encoding: JSONEncoding.default, headers: ServiceGenerator.getHttpHeader())
        .validate(statusCode: 200..<500)
            .response { (response) in
                let result = HTTPNetworkResponse.handleNetworkResponse(model: Matches.self, for: response)
                completion(result)
                
            }
    }
    
    func getCompetitions(_ completion: @escaping (Result<Any>) -> ()) {
        AF.request(HTTPNetworkRoute.COMPETITIONS, method: .get, encoding: JSONEncoding.default, headers: ServiceGenerator.getHttpHeader())
        .validate(statusCode: 200..<500)
            .response { (response) in
                let result = HTTPNetworkResponse.handleNetworkResponse(model: Competitions.self, for: response)
                completion(result)
                
            }
    }
    
    func getStanding(competitonId: Int ,_ completion: @escaping (Result<Any>) -> ()) {
        let url = "\(HTTPNetworkRoute.COMPETITIONS)/\(competitonId)/standings?standingType=HOME"
        
        AF.request(url, method: .get, encoding: JSONEncoding.default, headers: ServiceGenerator.getHttpHeader())
        .validate(statusCode: 200..<500)
            .response { (response) in
                let result = HTTPNetworkResponse.handleNetworkResponse(model: Standings.self, for: response)
                completion(result)
            }
    }
    
    func getTeams(competitonId: Int ,_ completion: @escaping (Result<Any>) -> ()) {
        let url = "\(HTTPNetworkRoute.COMPETITIONS)/\(competitonId)/teams"
        
        AF.request(url, method: .get, encoding: JSONEncoding.default, headers: ServiceGenerator.getHttpHeader())
        .validate(statusCode: 200..<500)
            .response { (response) in
                let result = HTTPNetworkResponse.handleNetworkResponse(model: CompetitionTeams.self, for: response)
                completion(result)
            }
    }
    
    func getPlayers(teamId: Int ,_ completion: @escaping (Result<Any>) -> ()) {
        let url = "\(HTTPNetworkRoute.TEAMS)/\(teamId)"
        
        AF.request(url, method: .get, encoding: JSONEncoding.default, headers: ServiceGenerator.getHttpHeader())
        .validate(statusCode: 200..<500)
            .response { (response) in
                let result = HTTPNetworkResponse.handleNetworkResponse(model: TeamSquad.self, for: response)
                completion(result)
            }
    }
    
    func getMatches(competitonId: Int , _ completion: @escaping (Result<Any>) -> ()) {
        let url = "\(HTTPNetworkRoute.COMPETITIONS)/\(competitonId)/matches"
        
        AF.request(url, method: .get, encoding: JSONEncoding.default, headers: ServiceGenerator.getHttpHeader())
        .validate(statusCode: 200..<500)
            .response { (response) in
                let result = HTTPNetworkResponse.handleNetworkResponse(model: Matches.self, for: response)
                completion(result)
                
            }
    }
}
