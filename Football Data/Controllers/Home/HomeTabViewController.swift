//
//  HomeTabViewController.swift
//  Football Data
//
//  Created by Oluwaniran Olorunfemi on 13/12/2020.
//

import UIKit

class MainTabVC : UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup() {
        let fixtureScreen = createNavBarItems(vc: FixturesViewController(nibName: nil, bundle: nil), unSelectedImage: "soccer" ,selectedImage: "soccer")
        
        let competitionsScreen = createNavBarItems(vc: CompetitionsViewController(nibName: nil, bundle: nil), unSelectedImage: "field" ,selectedImage: "field")
    
        self.viewControllers = [fixtureScreen, competitionsScreen]
        self.tabBar.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
        
        guard let items = tabBar.items else{return}
        for item in items {
            item.title = ""
            item.imageInsets = UIEdgeInsets(top: 6,left: 0,bottom: 6, right: 0)
        }
    }
}

extension UITabBarController {
    func createNavBarItems (vc: UIViewController, unSelectedImage: String, selectedImage: String) -> UINavigationController{
        let viewController = vc
        let createdNavItem = UINavigationController(rootViewController: viewController)
        createdNavItem.tabBarItem.image = UIImage(named:unSelectedImage)
        createdNavItem.tabBarItem.selectedImage = UIImage(named:selectedImage)
    
        return createdNavItem
    }
}
