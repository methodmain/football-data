//
//  ClubHeaderCell.swift
//  Football Data
//
//  Created by Oluwaniran Olorunfemi on 18/12/2020.
//

import UIKit

class ClubHeaderCell : UITableViewHeaderFooterView {
    
    @IBOutlet weak var clubLogo: UIImageView!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
}
