//
//  Match.swift
//  Football Data
//
//  Created by Oluwaniran Olorunfemi on 17/12/2020.
//

import Foundation

struct Match : Codable {
    var id : Int
    var matchDay : Int
    var date: String
    var homeTeam : Team
    var awayTeam : Team
    var score : Score
    //var currentScore : Score
    
    
    
    enum CodingKeys: String, CodingKey {
        case id
        case matchDay = "matchday"
        case date = "utcDate"
        case homeTeam
        case awayTeam
        case score
        //case currentScore = ""
    }
    
    func getCurrentScore() -> Goals {
        guard score.fullTime.homeTeam == nil else {
            //implies they have finished the match
            return score.fullTime
        }
        
        guard score.extraTime.homeTeam == nil else {
            //implies they are at extra time
            return score.extraTime
        }
        
        //return half time score
        return score.halfTime
    }
}

struct Matches : Codable {
    let count: Int
    let all: [Match]
    
    enum CodingKeys: String, CodingKey {
        case count
        case all = "matches"
    }
}


struct Team : Codable {
    var id : Int
    var name : String
}
