//
//  FixtureCell.swift
//  Football Data
//
//  Created by Oluwaniran Olorunfemi on 13/12/2020.
//

import UIKit

class FixtureCell: UICollectionViewCell {
    
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var homeTeamLabel: UILabel!
    
    @IBOutlet weak var awayTeamLabel: UILabel!
    
    @IBOutlet weak var homeTeamScoreLabel: UILabel!
    
    @IBOutlet weak var awayTeamScoreLabel: UILabel!
    
    @IBOutlet weak var timePlayedLabel: UILabel!
    
    @IBOutlet weak var matchDateLabel: UILabel!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
}
