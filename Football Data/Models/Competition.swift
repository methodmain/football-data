//
//  Competition.swift
//  Football Data
//
//  Created by Oluwaniran Olorunfemi on 17/12/2020.
//

struct Competition : Codable {
    var id : Int
    var name : String
}

struct Competitions : Codable {
    let count: Int
    let all: [Competition]
    
    enum CodingKeys: String, CodingKey {
        case count
        case all = "competitions"
    }
}
