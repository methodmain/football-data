//
//  Date.swift
//  Football Data
//
//  Created by Oluwaniran Olorunfemi on 18/12/2020.
//

import Foundation

extension Date {
    var formattedTime: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = NSLocale.is12HourFormat() ? "hh:mm" : "HH:mm"
        return dateFormatter.string(from: self)
    }
}
