//
//  Extensions.swift
//  Football Data
//
//  Created by Oluwaniran Olorunfemi on 17/12/2020.
//

import Foundation

extension String {
    func getDate(withFormat format: String = "yyyy-MM-dd'T'HH:mm:ssZ") -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        return dateFormatter.date(from: self)
    }
}
