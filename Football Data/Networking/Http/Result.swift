//
//  Result.swift
//  Football Data
//
//  Created by Oluwaniran Olorunfemi on 17/12/2020.
//

import Foundation
enum Result<T> {
    
    case success(T)
    case failure(Error)
    
    func get() -> T? {
        switch self {
        case .success(let data):
            return data
        default:
            return nil
        }
    }
}
