//
//  LeagueNavbarController.swift
//  Football Data
//
//  Created by Oluwaniran Olorunfemi on 14/12/2020.
//

import UIKit
import XLPagerTabStrip

class LeagueNavbarController : ButtonBarPagerTabStripViewController {
    
    var league : Competition?
    var table : [Standing]?
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let tableScreen = LeagueTableViewController(nibName: nil, bundle: nil)
        tableScreen.table = table
        
        let fixtureScreen = LeagueFixturesController(nibName: "Fixtures", bundle: nil)
        fixtureScreen.league = league
        
        let teamScreen = LeagueTeamsViewController(nibName: nil, bundle: nil)
        teamScreen.league = league
        
        return [tableScreen, fixtureScreen, teamScreen]
    }

    override func viewDidLoad() {
        configurePager()
        super.viewDidLoad()
        
        self.title = league?.name ?? ""
        self.navigationController?.navigationBar.isTranslucent = false
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.setValue(false, forKey: "hidesShadow")
    }

    func configurePager(){
        //background color
        settings.style.buttonBarBackgroundColor = .white
        settings.style.buttonBarItemBackgroundColor = .white
        // Sets the pager strip item offsets
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        // Sets the height and colour of the slider bar of the selected pager tab
        settings.style.selectedBarHeight = 2.0
        settings.style.selectedBarBackgroundColor = .black
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        // Changing item text color on swipe
        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
              guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = .gray
            newCell?.label.textColor = .black
        }

    }

}
